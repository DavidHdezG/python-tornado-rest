import asyncio

import tornado.web

class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.write("Hello from get")
    def post(self):
        self.write("Hello from post")
    def patch(self):
        self.write("Hello from patch")
    def delete(self):
        self.write("Hello from delete")
    def put(self):
        self.write("Hello from put")
def make_app():
    return tornado.web.Application([
        (r"/hello", MainHandler),
    ])

async def main():
    app = make_app()
    app.listen(8888)
    await asyncio.Event().wait()

if __name__ == "__main__":

    asyncio.run(main())